import elastic.peliculas.PeliculasSystem
import io.javalin.core.security.AccessManager
import io.javalin.core.security.Role
import io.javalin.http.Context
import io.javalin.http.Handler

enum class PeliculasRoles: Role {
    ANYONE
}


class PeliculasAccessManager(val instagramSystem: PeliculasSystem) : AccessManager {

    override fun manage(handler: Handler, ctx: Context, roles: MutableSet<Role>) {
        when {
            roles.contains(PeliculasRoles.ANYONE) -> handler.handle(ctx)
        }
    }
}