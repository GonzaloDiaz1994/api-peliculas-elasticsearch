package elastic.peliculas

class PeliculasSystem : PeliculaService{

    val dao = PeliculaDAO()

    override fun busquedaPorLucene(palabra: String): List<Pelicula> {
        return dao.busquedaLucene(palabra)
    }

    override fun busquedaEspecificaPorTitulo(titulo: String): List<Pelicula> {
        return dao.peliculasPorTitulo(titulo)
    }
}